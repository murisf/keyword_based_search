<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Keyword Based Search</title>
		<style>
			body {
				color: #333333;
				margin: 0 auto;
				font-size: 1em;
				font-family: Verdana, "Verdana CE", Arial, "Arial CE",
					"Lucida Grande CE", lucida, "Helvetica CE", sans-serif;
				text-align: center;
			}
			
			table {
				width: 75%;
				align: center;
			}

			td, tr {
				border: 1px solid lightblue;
			}
			
			.indiv {
				position: relative;
				width: 27em;
				height: 10em;
				padding: 1em;
				overflow: auto;
			}
			
			.toprow {
				bgcolor: #ca502f;
				text-align: center;
				colspan: 4;
			}
		</style>
	</head>
	<body>
		<form id="keywordsearch" method="get" action="ContentServlet">
			<input type="text" name="bar" id="bar" size="21" maxlength="120" placeholder="Keywords for theory">
			<input type="submit" value="Search">
		</form>
		<table>
			<tr>
				<td align="center" colspan="4" bgcolor="#CA502F">Search Results</td>
			</tr>
			<c:forEach items="${list}" var="list">
				<tr>
					<td width="10%"><img src="img/plus.png"></td>
					<td width="10%">${list.count}</td>
					<td width="50%">${list.sourceId} ${list.bookTitle} (${list.authors})</td>
					<td width="30%">${list.edition}/${list.year}</td>
				</tr>
			</c:forEach>
			<tr>
				<td align="center" colspan="4" bgcolor="#CA502F">The End</td>
			</tr>
		</table>
	</body>
</html>