package edu.umsl.keyword.beans;

import java.sql.*;
import java.util.*;

public class KeywordDataBean
{
	private Connection connection;
	private PreparedStatement getTheories;
	
	public KeywordDataBean(String keyword) throws Exception
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/keyworddb", "root", "");
			
			
			getTheories = connection.prepareStatement("SELECT kid FROM keyword WHERE keyword_name = ?");
			getTheories.setString(1, keyword);
		}
		catch (SQLException sqle)
		{
			sqle.printStackTrace();
		}
	}
	
	public List<KeywordBean> getKeywords() throws SQLException
	{
		int i = 0, count = 0;
		List<KeywordBean> list = new LinkedList<KeywordBean>();
		List<Integer> idList = new ArrayList<Integer>();
		KeywordBean bean = new KeywordBean();
		ResultSet results = getTheories.executeQuery();

		while(results.next())
		{
			bean.setKid(results.getInt(1));
		}
		
		getTheories = connection.prepareStatement("SELECT item_id FROM single_keyword_mapping WHERE keyword_id = ?");
		getTheories.setInt(1,  bean.getKid());
		
		results = getTheories.executeQuery();

		while(results.next())
		{
			KeywordBean bean2 = new KeywordBean();
			bean2.setKid(bean.getKid());
			bean2.setItemId(results.getInt(1));
			list.add(bean2);
		}
		
		getTheories = connection.prepareStatement("SELECT source_id, content FROM my_theory WHERE mtid = ?");
		
		while (i < list.size())
		{
			getTheories.setInt(1, list.get(i).getItemId());
			results = getTheories.executeQuery();
			
			while(results.next())
			{
				if (idList.contains(results.getInt(1)))
				{
					list.remove(i);
					i--;
					count++;
				}
				else
				{
					
					idList.add(results.getInt(1));
					list.get(i).setSourceId(results.getInt(1));
					list.get(i).setContent(results.getString(1));
					count = 0;
				}
				list.get(i).setCount(count + 1);
			}
			i++;
		}

		getTheories = connection.prepareStatement("SELECT book_title, authors, edition_number, year_number FROM book WHERE bid = ?");
		
		i = 0;
		while (i < list.size())
		{	
			getTheories.setInt(1,  list.get(i).getSourceId());
			results = getTheories.executeQuery();
			
			while(results.next())
			{
				list.get(i).setBookTitle(results.getString(1));
				list.get(i).setAuthor(results.getString(2));
				list.get(i).setEdition(results.getInt(3));
				list.get(i).setYear(results.getInt(4));
			}
			i++;
		}
		
		return list;
	}
	
	protected void finalize()
	{
		try
		{
			getTheories.close();
		}
		catch (SQLException sqle)
		{
			sqle.printStackTrace();
		}
	}
}
