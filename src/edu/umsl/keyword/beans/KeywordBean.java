package edu.umsl.keyword.beans;

public class KeywordBean
{
	private String keyword, content, bookTitle, authors;
	private int count, edition, year, kid, itemid, sourceid;

	public void setKeyword(String name )
	{
		keyword = name;  
	}
	   
	public String getKeyword()
	{
		return keyword;  
	}
	   
	public void setContent(String name)
	{
		content = name;
	}
	   
	public String getContent()
	{
		return content;
	}
	   
	public void setCount(int num)
	{
		count = num;
	}
	
	public int getCount()
	{
		return count;
	}
	
	public void setKid(int num)
	{
		kid = num;
	}
	
	public int getKid()
	{
		return kid;
	}
	
	public void setItemId(int num)
	{
		itemid = num;
	}
	
	public int getItemId()
	{
		return itemid;
	}
	
	public void setSourceId(int num)
	{
		sourceid = num;
	}
	
	public int getSourceId()
	{
		return sourceid;
	}
	
	public void setBookTitle(String title)
	{
		bookTitle = title;
	}
	
	public String getBookTitle()
	{
		return bookTitle;
	}
	
	public void setAuthor(String author)
	{
		authors = author;
	}
	
	public String getAuthors()
	{
		return authors;
	}
	
	public void setYear(int year)
	{
		this.year = year;
	}
	
	public int getYear()
	{
		return year;
	}
	
	public void setEdition (int num)
	{
		edition = num;
	}
	
	public int getEdition()
	{
		return edition;
	}
}
