package edu.umsl.keyword.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.umsl.keyword.beans.*;

@WebServlet("/ContentServlet")
public class ContentServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
       
    public ContentServlet()
    {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		String keyword = request.getParameter("bar");
		if (keyword.contains(","))
			keyword = keyword.substring(0, keyword.indexOf(","));
			
		RequestDispatcher dispatcher = 
                request.getRequestDispatcher("index.jsp");
		
		try
		{
			KeywordDataBean bean = new KeywordDataBean(keyword);
			List<KeywordBean> list = new ArrayList<KeywordBean>();
			list = bean.getKeywords();
			request.setAttribute("list", list);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		dispatcher.forward(request, response);
	}
}
